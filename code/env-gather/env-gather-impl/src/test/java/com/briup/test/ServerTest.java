package com.briup.test;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.briup.smart.env.server.Server;
import com.briup.smart.env.server.ServerImpl;

public class ServerTest<Environment> implements Server {
	
	private ExecutorService pool = null;
	private ServerSocket server = null;
	private boolean flag=true;
	
	@Override
	public void reciver() throws Exception {
		
		
		// 线程池对象， 实现多个客户端并发请求
		pool = Executors.newFixedThreadPool(5);
		

		server = new ServerSocket(9999);
		// main线程进入循环状态
		while (flag) {
			System.out.println("客户端9999等待客户端连接");
			// 调用accept方法接收客户端连接
			Socket socket = server.accept();
			System.out.println("客户端9999连接成功");
			pool.execute(() -> {

				try {
					// 获取输入流从而获取集合对象
					InputStream is = socket.getInputStream();
					ObjectInputStream ois = new ObjectInputStream(is);

					Collection<Environment> coll = (Collection<Environment>) ois.readObject();
					String name = Thread.currentThread().getName();
					System.out.println(name + " 接收到数据：" + coll.size());
				} catch (Exception e) {

					e.printStackTrace();

				}

			});
		}
	}
	/*
	 * shutdown 与reciver同时调用
	 * reciver 阻塞无法shutdown
	 * shutdown reciver没有创建服务器
	 */

	@Override
	public void shutdown() throws Exception {
		//新的线程  创建新的server服务器实现停止服务器
		new Thread(()->{
			//创建新的server服务器实现停止服务器
			try {
				ServerSocket server=new ServerSocket(8888);
				System.out.println("客户端8888准备连接，开启停止服务器");
				//连接成功，表示停止接收服务器		
				server.accept();
				System.out.println("客户端8888连接成功开始关闭接收服务器");
				if(this.server != null)this.server.close();
				if(this.pool != null)this.pool.shutdown();
				this.flag = false;
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		

				
		}).start();

	} 

	public static void main(String[] args) throws Exception {
		
		
        ServerTest server=new ServerTest();
        //开启停止服务器，等待停止指令
        server.shutdown();  
      
         //
        try {
        	//接收客户端数据
        	server.reciver(); 
        	
        }catch (Exception e) {
			System.out.println("关闭服务器成功");
		}
	}

}
