package com.briup.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import com.briup.smart.env.client.Gather;
import com.briup.smart.env.entity.Environment;

public class GatherTest2 implements Gather{

	public Collection<Environment> gather() throws Exception {
		Collection<Environment> list = new ArrayList<Environment>();
		// 1读取数据文件 ，获取到每一个数据
		BufferedReader bf = new BufferedReader(new FileReader("D:\\data-file"));

		// 2对每行数据 字符进行拆分 |
		String string = null;
		while ((string = bf.readLine()) != null) {
			// 2.将读取的数据进行拆分
			String[] data = string.split("\\|");

			// 3.创建环境对象，将环境对象进行赋值
			Environment en = new Environment();
			// 通过init方法对环境对象进行赋值操作
			init(data, en);
			switch (data[3]) {
			case "16":
				en.setName("温度");
				en.setData(Integer.parseInt(data[6].substring(0, 4), 16) * 0.00268127F - 46.85F);
				// 如何保存湿度信息
				Environment en2 = new Environment();
				init(data, en2);
				en2.setName("湿度");
				en2.setData(Integer.parseInt(data[6].substring(4, 8), 16) * 0.00190735F - 6);
				list.add(en2);
				break;
			case "256":
				en.setName("光照强度");
				en.setData(Integer.parseInt(data[6].substring(0, 4), 16));
				break;
			case "1280":
				en.setName("二氧化碳浓度");
				en.setData(Integer.parseInt(data[6].substring(0, 4), 16));
				break;
			default:
				break;
			}
			// 将环境对象添加到集合中
			list.add(en);// 温度 co2 光照

		}
		return list;
	}

	/**
	 * 通过拆分字符串数组对环境对象进行赋值的操作 data 
	 * 拆分字符串 en 
	 */

	private void init(String[] data, Environment en) {

		en.setSrcId(data[0]);
		en.setDesId(data[1]);
		en.setDevId(data[2]);
		en.setSersorAddress(data[3]);
		en.setCount(Integer.parseInt(data[4]));
		en.setCmd(data[5]);
		// data[6]环境数值
		en.setStatus(Integer.parseInt(data[7]));
		en.setGather_date(new Timestamp(Long.parseLong(data[8])));
	}
	public static void main(String[] args) throws Exception {
		Collection<Environment> list=new GatherTest2().gather();
		System.out.println(list.size());
		//统计温度对象 湿度对象个数
		long count= list.stream()
		.filter(e -> e.getName().equals("温度"))
		.count();
		System.out.println(count);
		long count2= list.stream()
				.filter(e -> e.getName().equals("湿度"))
				.count();
		System.out.println(count2);
		//输出
		long   num= list.stream().filter(e -> e.getName().equals("温度")).count();
		System.out.println(num);
		long  num2= list.stream().filter(e -> e.getName().equals("湿度")).count();
		System.out.println(num2);
		list.stream().filter(e -> e.getName().equals("湿度")).forEach(e -> System.out.println(e));

		
	}
	

	}


