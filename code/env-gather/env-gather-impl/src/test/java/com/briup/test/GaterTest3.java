package com.briup.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import com.briup.smart.env.client.Gather;
import com.briup.smart.env.entity.Environment;

public class GaterTest3 implements Gather{

	@Override
	public Collection<Environment> gather() throws Exception {
		Collection<Environment> list = new ArrayList();
		//1.读取文件读取每一行 相对路径 target/classes  
		String path = "data-file";
		BufferedReader br = new BufferedReader(new FileReader("D:\\data-file"));
		String line = null;
		while((line = br.readLine()) != null) {
			//2.将读取的数据进行拆分
			String[] data = line.split("[|]");
			//3.创建环境对象，将环境对象进行赋值
			Environment en = new Environment();
			//通过set方法将环境对象进行赋值操作
			en.setSrcId(data[0]);
			en.setDesId(data[1]);
			en.setDevId(data[2]);
			en.setSersorAddress(data[3]);
			en.setCount(Integer.parseInt(data[4]));
			en.setCmd(data[5]);
			//data[6] : 环境数值
			en.setStatus(Integer.parseInt(data[7]));
			en.setGather_date(new Timestamp(Long.parseLong(data[8])));
			//根据传感器地址分别处理name和data属性
			// if  if else  switch 
			switch (data[3]) {
			case "16":
				Environment en2 = copyObj(en);
				
				en.setName("温度");
				en.setData(Integer.parseInt(data[6].substring(0, 4), 16)*0.00268127F-46.85F);
				//如何保存湿度信息
				//如何对en2这个对象进行赋值操作
				//利用en对象复制出一个en2的对象 
				en2.setName("湿度");
				en2.setData(Integer.parseInt(data[6].substring(4, 8), 16)*0.00190735F-6);
				list.add(en2);
				break;
			case "256":
				en.setName("光照强度");
				en.setData(Integer.parseInt(data[6].substring(0, 4),16));
				break;
			case "1280":
				en.setName("二氧化碳浓度");
				en.setData(Integer.parseInt(data[6].substring(0, 4),16));
				break;
			default:
				System.out.println("未知类型");//警告日志
				break;
			}
			list.add(en);// 温度  co2 光照
		}
		    return list;
	}
		/**
		 * 通过参数对象实现复制操作
		 * @param en  可以复制的对象
		 * @return  新的对象
		 * @throws Exception 
		 */
		public Environment copyObject(Environment en) throws Exception {
			Class<? extends Environment> c = en.getClass();
			//1.创建对象
			Environment copy = c.newInstance();
			//2.进行赋值: 反射： Field类型 Method
			Field[] fields = c.getDeclaredFields();
			for (Field field : fields) {
				//将final修饰属性排除
				if(! Modifier.isFinal(field.getModifiers())) {
					field.setAccessible(true);
					Object value = field.get(en);
					field.set(copy, value);
				}
				
			}
			return copy;

	}
		public <T> T copyObj(T t) throws Exception {
			Class<T> c = (Class<T>) t.getClass();
			T copy = c.newInstance();
			Field[] fields = c.getDeclaredFields();
			for (Field field : fields) {
				//将final修饰属性排除
				if(! Modifier.isFinal(field.getModifiers())) {
					field.setAccessible(true);
					Object value = field.get(t);
					field.set(copy, value);
				}
				
			}
			return copy;
		}
		public static void main(String[] args) throws Exception {
			new GaterTest3().copyObject(new Environment());
			
			Collection<Environment> list = new GaterTest3().gather();
			System.out.println(list.size());
			//统计代码和业务代码分离 
			long num = list.stream()
			.filter(e -> e.getName().equals("温度"))
			.count();
			System.out.println(num);
			long num2= list.stream()
					.filter(e -> e.getName().equals("湿度"))
					.count();
			System.out.println(num2);
			list.stream()
			.filter(e -> e.getName().equals("湿度"))
			.forEach(e -> System.out.println(e));
			
		}


}
	


