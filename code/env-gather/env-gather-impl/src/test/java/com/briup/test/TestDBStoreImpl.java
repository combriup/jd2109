package com.briup.test;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.alibaba.druid.support.logging.Log;
import com.briup.smart.env.Configuration;
import com.briup.smart.env.client.GatherImpl;
import com.briup.smart.env.entity.Environment;
import com.briup.smart.env.server.DBStore;
import com.briup.smart.env.support.ConfigurationAware;

// 1集合对象通过服务器获取
// 2遍历集合对象，获取每个环境对象
// 3执行sql语句，将对象保存e_detail_X
/*
 * 预编译对象（ps+sql)+批处理（2）+手动提交事务（2）+回滚（1） +循环遍历（2）+set 获取 数据存储 分表存储 获取环境对象 ----> y
 * 号 同构sql sql ？？？ e_detail_y 批处理 ps.add ps.exe +判断逻辑 conn连接 多个不同批处理 多次事务提交
 * 
 */
public class TestDBStoreImpl implements DBStore, ConfigurationAware {
	private static DataSource pool;
	// list集合：可以 下标值 0-31 元素：ps
	private static Map<Integer, PreparedStatement> prepMap;
	private static List<PreparedStatement> list;
	private Log logger;
	static {
		try {
			// 1.读取配置文件
			InputStream in = TestDBStoreImpl.class.getClassLoader().getResourceAsStream("Druid.properties");
			Properties prop = new Properties();
			prop.load(in);
			// 2.数据库连接池druid利用log4j.jar实现日志记录功能
			pool = DruidDataSourceFactory.createDataSource(prop);

			Connection conn = pool.getConnection();
			prepMap = new HashMap<>();
			list = new ArrayList<>();
			for (int i = 1; i <= 31; i++) {
				String sql = "insert into e_detail_" + i
						+ "(name,srcId,desId,devId,sersorAddress,count,cmd,status,data,gather_date) values(?,?,?,?,?,?,?,?,?,?)";
				PreparedStatement ps = conn.prepareStatement(sql);
				prepMap.put(i, ps);
				list.add(ps);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	/*
	 * ps+批处理 1 day=1 新的ps对象 add 1 day=1 if(1==1) 使用上一个ps add 1 day=1 if(1==1)
	 * 使用上一个ps add 2 day=2 if(2!=1) exec上一次批处理 新的ps对象 add 2 3 day=3 if(3!=2)
	 * exec上一次批处理 新的ps对象 add 3
	 * 
	 * 
	 * 
	 * 
	 * 
	 */

	@Override
	public void saveDB(Collection<Environment> c) {
		// 提供一个容器保存day=num之间的关系
		HashMap<Integer, Integer> map = new HashMap<>();
		// 1.获取连接对象
		Connection conn = null;
		try {
			conn = pool.getConnection();
			conn.setAutoCommit(false);// 设置事务手动提交
			// 2.遍历集合对象c
			PreparedStatement ps = null;
			int day = 0;// 表示当前环境对象表示的时间： X号
			int flag = 0;// 表示上一次创建ps对象的一个时间
			int count = 0;// 表示同一批中添加的环境数据值
			for (Environment en : c) {// 10万
				day = en.getGather_date().toLocalDateTime().getDayOfMonth();
				// 3.利用预编译和批处理代码插入数据
				if (day != flag) {// 当读取环境数据与上一条环境数据不在同一天
					if (ps != null) {
						map.put(flag, count);
						// 将上一次某个日期的一批数据提交到数据库
						ps.executeBatch();
					}
					// 重复利用集合中保存的ps对象
					ps = prepMap.get(day);
					ps = list.get(day - 1);
					// 将标志设置为当前时间
					flag = day;
					// 对count进行清除,重新统计同一天的数据
					count = 0;
				}
				// 4.设值
				ps.setString(1, en.getName());
				ps.setString(2, en.getSrcId());
				ps.setString(3, en.getDesId());
				ps.setString(4, en.getDevId());
				ps.setString(5, en.getSersorAddress());
				ps.setInt(6, en.getCount());
				ps.setString(7, en.getCmd());
				ps.setInt(8, en.getStatus());
				ps.setFloat(9, en.getData());
				ps.setTimestamp(10, en.getGather_date());
				// 5.将值添加到批处理中
				ps.addBatch();
				count++;
				// 添加的数量 ： 10万 都是同一天 10万
				if (count % 10000 == 0) {// 一直进行批处理添加，超过1万条进行一次提交
					ps.executeBatch();
				}
			}
			// 6.执行批处理: 执行最后日期的环境数据
			ps.executeBatch();
			map.put(flag, count);
			conn.commit();
		} catch (Exception e) {
			try {
				conn.rollback();// 回滚
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		//  统计 每个日期对应的条数
		System.out.println("统计数据：" + map);
	}

	public static void main(String[] args) throws Exception {
		Collection<Environment> c = new GatherImpl().gather();
		new TestDBStoreImpl().saveDB(c);
		// 集合中某天数据：
		long num = c.stream().filter(e -> e.getGather_date().toLocalDateTime().getDayOfMonth() == 23).count();
		System.out.println(num);// 1735

	}

	/*
	 * 1.如何实现? A模块依赖B模块： 通过配置模块获取B模块的对象 2.如何调用？ 在创建对象，让配置模块自动调用方法
	 */
	@Override
	public void setConfiguration(Configuration configuration) throws Exception {

	}

}
