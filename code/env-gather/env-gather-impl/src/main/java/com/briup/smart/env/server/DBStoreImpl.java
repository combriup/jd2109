package com.briup.smart.env.server;

import java.io.File;
import java.io.IOException;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Properties;

import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.briup.smart.env.Configuration;
import com.briup.smart.env.ConfigurationImpl;
import com.briup.smart.env.client.Gather;
import com.briup.smart.env.client.GatherImpl;
import com.briup.smart.env.entity.Environment;
import com.briup.smart.env.support.ConfigurationAware;
import com.briup.smart.env.support.PropertiesAware;
import com.briup.smart.env.util.Backup;
import com.briup.smart.env.util.Log;
import com.briup.smart.env.util.LogImpl;

/**
 * 入库模块的实现类
 * 备份： 1.当异常发生时，将集合对象保存的到文件中
 *      2.当下一次读取，先读取备份中集合对象，然后入库
 * 
 */
public class DBStoreImpl implements DBStore,ConfigurationAware,PropertiesAware {
	private String driver;
	private String url;
	private String user;
	private String password;
	private String dbBackupFile;
	private static DataSource pool;
	
	
	private Log logger;
	private Backup backup;
	static {
		try {
			// 1.读取配置文件
			/*
			 * InputStream in =
			 * DBStoreImpl.class.getClassLoader().getResourceAsStream("Druid.properties");
			 * Properties prop = new Properties(); prop.load(in);
			 */
			// 2.数据库连接池druid利用log4j.jar实现日志记录功能
			/* pool = DruidDataSourceFactory.createDataSource(prop); */
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*
	 * ps+批处理 1 day=1 新的ps对象 add 1 day=1 if(1==1) 使用上一个ps add 1 day=1 if(1==1)
	 * 使用上一个ps add 2 day=2 if(2!=1) exec上一次批处理 新的ps对象 add 2 3 day=3 if(3!=2)
	 * exec上一次批处理 新的ps对象 add 3
	 */
	@Override
	public void saveDB(Collection<Environment> c) {
           logger.info("开始执行入库操作");
		// 1.获取连接对象
		Connection conn = null;
		try {
			conn = pool.getConnection();
			conn.setAutoCommit(false);// 设置事务手动提交
			// 2.遍历集合对象c
			PreparedStatement ps = null;
			int day = 0;// 表示当前环境对象表示的时间： 号
			int flag = 0;// 表示上一次创建ps对象的一个时间
			int count = 0;// 表示同一批中添加的环境数据值
			//当备份文件存在时，进行恢复备份操作
			if(new File(dbBackupFile).exists()) {
				//下一次进入入库操作：先加载备份数据
				Collection<Environment> backList = (Collection<Environment>) backup.load(dbBackupFile, Backup.LOAD_REMOVE);
				c.addAll(backList);
				System.out.println("备份数据："+backList.size());
			
			}
			for (Environment en : c) {// 10万
				day = en.getGather_date().toLocalDateTime().getDayOfMonth();
				// 3.利用预编译和批处理代码插入数据
				if (day != flag) {// 当读取环境数据与上一条环境数据不在同一天
					if (ps != null) {
						// 将上一次某个日期的一批数据提交到数据库
						ps.executeBatch();
					}
String sql = "insert into e_detail_" + day + "(name,srcId,desId,devId,sersorAddress,count,cmd,status,data,gather_date) values(?,?,?,?,?,?,?,?,?,?)";
					ps = conn.prepareStatement(sql);
					// 将标志设置为当前时间
					flag = day;
					// 对count进行清除,重新统计同一天的数据
					count = 0;
				}
				// 4.设值
				ps.setString(1, en.getName());
				ps.setString(2, en.getSrcId());
				ps.setString(3, en.getDesId());
				ps.setString(4, en.getDevId());
				ps.setString(5, en.getSersorAddress());
				ps.setInt(6, en.getCount());
				ps.setString(7, en.getCmd());
				ps.setInt(8, en.getStatus());
				ps.setFloat(9, en.getData());
				ps.setTimestamp(10, en.getGather_date());
				
				// 5.将值添加到批处理中
				ps.addBatch();
				count++;
				//当数量是10时，产生异常 
				if(count == 10) {
					//测试备份模块使用
				//	throw new RuntimeException("模拟入库异常");
				}
				// 进行批处理添加，超过1万条进行一次提交
				if (count % 10000 == 0) {
					ps.executeBatch();
				}
			}
			// 6.执行批处理: 执行最后日期的环境数据
			if(ps!=null) {
			ps.executeBatch();
			}
			conn.commit();
		} catch (Exception e) {
			try {
				logger.error("入库操作发送错误:"+e.getMessage());
				//启动备份功能： 将集合对象保存在备份文件 
				backup.store(dbBackupFile, c, Backup.STORE_OVERRIDE);
				conn.rollback();// 回滚
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		logger.info("完成入库操作");
	//	System.out.println("入库模块发送成功");
		
	}
	private void initPool() throws Exception{
		//将4个数据库连接信息加载到properties
		Properties prop=new Properties();
		prop.setProperty("driverClassName", driver);
		prop.setProperty("url", url);
		prop.setProperty("username", user);
		prop.setProperty("password", password);
		InputStream in=DBStoreImpl.class.getClassLoader().getResourceAsStream("Druid.properties");
		prop.load(in);
		//2数据库连接池druid利用log4j.jar实现日志记录功能
		pool=DruidDataSourceFactory.createDataSource(prop);
		
	}
	@Override
	public void init(Properties properties) throws Exception {
		this.driver = properties.getProperty("driver");
		this.url = properties.getProperty("url");
		this.user = properties.getProperty("username");
		this.password = properties.getProperty("password");
		this.dbBackupFile = properties.getProperty("db-backup-file");
		initPool();
	}
	@Override
	public void setConfiguration(Configuration configuration) throws Exception {
		this.logger = configuration.getLogger();
		this.backup = configuration.getBackup();
	}

	public static void main(String[] args) throws Exception {
		
		/*
		 * Collection<Environment> c = new GatherImpl().gather(); new
		 * DBStoreImpl().saveDB(c); // 集合中某天数据： long num = c.stream().filter(e ->
		 * e.getGather_date().toLocalDateTime().getDayOfMonth() == 23).count();
		 * System.out.println(num);// 1735 // 扩展： 提供一个统计功能： 统计 每个日期对应的条数
		 */		 
			
			  Configuration config = ConfigurationImpl.instance(); 
			  Gather gather =config.getGather(); 
			  Collection<Environment> c = gather.gather(); 
			  DBStore db =config.getDbStore(); 
			  db.saveDB(c);
			 
	}

}
