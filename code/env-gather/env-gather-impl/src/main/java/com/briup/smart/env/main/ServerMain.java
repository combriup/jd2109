package com.briup.smart.env.main;

import com.briup.smart.env.Configuration;
import com.briup.smart.env.ConfigurationImpl;
import com.briup.smart.env.server.Server;
import com.briup.smart.env.util.Log;

//服务器入口类
public class ServerMain {
	
	public static void main(String[] args) {
		Log log=null;
		
		try {
			Configuration config=ConfigurationImpl.instance();
			log=config.getLogger();
			Server server=config.getServer();
			server.shutdown();
			server.reciver();
		} catch (Exception e) {
			
			log.info("接收服务器关闭");
		}
	}
	
}
