package com.briup.smart.env.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Properties;
import java.util.stream.Stream;

import org.dom4j.Branch;
import org.hamcrest.core.SubstringMatcher;

import com.briup.smart.env.Configuration;
import com.briup.smart.env.ConfigurationImpl;
import com.briup.smart.env.entity.Environment;
import com.briup.smart.env.support.ConfigurationAware;
import com.briup.smart.env.support.PropertiesAware;
import com.briup.smart.env.util.Backup;
import com.briup.smart.env.util.Log;
/**
 * 采集模块  依赖日志模块
 * 采集模块实现类，实现读取数据文件信息封装成集合对象
 * 
 * 
 */

public class GatherImpl implements Gather, ConfigurationAware, PropertiesAware {

	private Log logger;
	private String path;
	private Backup backup;
	private String backupFile;

	@Override
	public Collection<Environment> gather() throws Exception {
		Collection<Environment> list = new ArrayList<Environment>();
		logger.info("客户端开始采集数据");
		// 1读取数据文件 ，获取到每一个数据
		// String path = "data-file";
		// BufferedReader bf = new BufferedReader(new FileReader("D:\\data-file"));

		InputStream in = GatherImpl.class.getClassLoader().getResourceAsStream(path);
		int count = in.available();// 读取的文件所有字节长度
		BufferedReader bf = new BufferedReader(new InputStreamReader(in));
		System.out.println(bf.readLine());
		// 存在备份数据
		if (new File(backupFile).exists()) {
			// 保存的上次读取文件的字节长度信息
			int num = (int) backup.load(backupFile, Backup.LOAD_REMOVE);// 读取文件同时备份文件
			bf.skip(num);// 跳过上次读取过的数据
		}

		// 2对每行数据 字符进行拆分 |
		String string = null;
		int num = 0;
		while ((string = bf.readLine()) != null) {
			System.out.println("|" + string + "|");
			num++;
			// 2.将读取的数据进行拆分
			String[] data = string.split("[|]");
			if (data.length != 9) {
				// 跳过：新加入的行中 多添加换行符
				// 防止data[x]下标越界异常
				continue;
			}

			// 3.创建环境对象，将环境对象进行赋值
			Environment en = new Environment();
			// 通过init方法对环境对象进行赋值操作
			init(data, en);
			switch (data[3]) {
			case "16":
				en.setName("温度");
				en.setData(Integer.parseInt(data[6].substring(0, 4), 16) * 0.00268127F - 46.85F);
				// 保存湿度信息
				Environment en2 = new Environment();
				init(data, en2);
				en2.setName("湿度");
				en2.setData(Integer.parseInt(data[6].substring(4, 8), 16) * 0.00190735F - 6);
				list.add(en2);
				break;
			case "256":
				en.setName("光照强度");
				en.setData(Integer.parseInt(data[6].substring(0, 4), 16));
				break;
			case "1280":
				en.setName("二氧化碳浓度");
				en.setData(Integer.parseInt(data[6].substring(0, 4), 16));
				break;
			default:
				logger.error("采集数据类型错误："+en);
				break;
			}
			// 将环境对象添加到集合中
			list.add(en);// 温度 co2 光照

		}
		logger.info("客户端采集数据结束");
		// 启动备份功能：备份已经读取成功的历史数据
		backup.store(backupFile, count, Backup.STORE_OVERRIDE);
		System.out.println("行数：" + num);
		return list;
	}

	/**
	 * 通过拆分字符串数组对环境对象进行赋值的操作 data 拆分字符串 en
	 */

	private void init(String[] data, Environment en) {

		en.setSrcId(data[0]);
		en.setDesId(data[1]);
		en.setDevId(data[2]);
		en.setSersorAddress(data[3]);
		en.setCount(Integer.parseInt(data[4]));
		en.setCmd(data[5]);
		// data[6]环境数值
		en.setStatus(Integer.parseInt(data[7]));
		en.setGather_date(new Timestamp(Long.parseLong(data[8])));
	}

	@Override
	public void init(Properties properties) throws Exception {
		this.path = properties.getProperty("data-file-path");
		this.backupFile = properties.getProperty("backup-file-path");

	}

	@Override
	public void setConfiguration(Configuration configuration) throws Exception {
		this.logger = configuration.getLogger();
		this.backup = configuration.getBackup();

	}

	public static void main(String[] args) throws Exception {
		/*
		 * Collection<Environment> list = new GatherImpl().gather();
		 * System.out.println(list.size()); // 统计温度对象 湿度对象个数 long count =
		 * list.stream().filter(e -> e.getName().equals("温度")).count();
		 * System.out.println(count); long count2 = list.stream().filter(e ->
		 * e.getName().equals("湿度")).count(); System.out.println(count2); long count3 =
		 * list.stream().filter(e -> e.getName().equals("光照强度")).count();
		 * System.out.println(count3); long count4 = list.stream().filter(e ->
		 * e.getName().equals("二氧化碳浓度")).count(); System.out.println(count4);
		 */

		// 测试采集模块中备份功能
		Gather gather = ConfigurationImpl.instance().getGather();
		Collection<Environment> c = gather.gather();
		// 数据文件是动态的
		System.out.println("环境对象：" + c.size());
	}

}