package com.briup.smart.env.util;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.briup.smart.env.Configuration;
import com.briup.smart.env.support.ConfigurationAware;
import com.briup.smart.env.support.PropertiesAware;
/**
 * 日志模块：
 * 1.实现采集模块开发者需不需学习log4j
 *   new LogImpl().debug(XXXX);
 *   实现其他模块与log4j解耦 
 * 2.项目升级 ：更换日志实现的框架。只需要修改日志实现类的代码
 * 缺点： 
 * 每一个其他模块和日志模块产生耦合的作用
 * 高内聚 ： 封装的一个方法 
 * 低耦合 ： 耦合问题： 配置模块 
 * @author lining
 * 使用： 将所有项目中使用的输出语句替换为日志模块的方法
 * debug.....
 * 统计信息：debug
 * 开启服务器： info
 * catch(){
 * 	 error(....)
 *   info(....)
 * }
 */
public class LogImpl implements Log,ConfigurationAware,PropertiesAware{
	private static Logger logger=Logger.getRootLogger();
	

	@Override
	public void debug(String message) {
	  logger.debug(message);
		
	}

	@Override
	public void info(String message) {
		logger.info(message);
		
	}

	@Override
	public void warn(String message) {
		
		logger.warn(message);
	}

	@Override
	public void error(String message) {
		logger.error(message);
		
	}

	@Override
	public void fatal(String message) {
		logger.fatal(message);
		
	}

	@Override
	public void init(Properties properties) throws Exception {
		
	}

	@Override
	public void setConfiguration(Configuration configuration) throws Exception {
	
	}

}
