package com.briup.smart.env.main;

import java.util.Collection;

import com.briup.smart.env.Configuration;
import com.briup.smart.env.ConfigurationImpl;
import com.briup.smart.env.client.Client;
import com.briup.smart.env.entity.Environment;
import com.briup.smart.env.util.Log;

//客户端入口类
public class ClientMain {
	public static void main(String[] args) {
		Log log=null;
		
		//1通过配置模块对象获取客户端模块对象，调用发送方法
		try {
			Configuration config=ConfigurationImpl.instance();
			log=config.getLogger();
			Client client=config.getClient();
			Collection<Environment> c=config.getGather().gather();
			client.send(c);
		} catch (Exception e) {
			
			log.error("客户端错误："+e.getMessage());
		}
		
	}
}
