package com.briup.smart.env.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * java IO流实现  对象的保存和读取
 * @author Lenovo
 * 采集模块中使用
 *入库模块中使用
 */

public class BackupImpl implements Backup {

	@Override
	public Object load(String fileName, boolean del) throws Exception {
		//读取文件
		ObjectInputStream ois=new ObjectInputStream(new FileInputStream(fileName));
		Object obj=ois.readObject();
		//读取后删除文件File类  删除
		return obj;
	}

	@Override
	public void store(String fileName, Object obj, boolean append) throws Exception {
		//写出
		ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream(fileName,append));
		oos.writeObject(obj);
		oos.flush();
		oos.close();
	}

}
