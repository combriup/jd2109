package com.briup.smart.env.client;

import static org.hamcrest.CoreMatchers.nullValue;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Collection;
import java.util.Properties;

import com.briup.smart.env.Configuration;
import com.briup.smart.env.entity.Environment;
import com.briup.smart.env.support.ConfigurationAware;
import com.briup.smart.env.support.PropertiesAware;
import com.briup.smart.env.util.Log;

/**
 * 客户端：依赖日志模块
 * 
 */
public class ClientImpl implements Client, ConfigurationAware, PropertiesAware {
	private Log logger;
	private String host;
	private int port;

	@Override
	public void send(Collection<Environment> c) throws Exception {
		logger.info("开启客户端");
		// 1.创建一个socket对象表示客户端

		Socket socket = new Socket(host, port);
		// 2.通过soket对象获取到一个流对象
		OutputStream os = socket.getOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(os);

		oos.writeObject(c);

		oos.flush();
		oos.close();
		
		socket.close();
		logger.info("发送采集数据成功");
	}

	@Override
	public void init(Properties properties) throws Exception {
		this.port = Integer.parseInt(properties.getProperty("port"));
		this.host = properties.getProperty("host");
	}

	@Override
	public void setConfiguration(Configuration configuration) throws Exception {
		this.logger = configuration.getLogger();
	}

	/*
	 * public static void main(String[] args) throws Exception { // 获取采集模块的集合对象
	 * Collection<Environment> c = new GatherImpl().gather(); // 3.将采集模块中的集合对象发送到服务端
	 * 
	 * new ClientImpl().send(c); }
	 */

}