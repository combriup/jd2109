package com.briup.smart.env.server;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collection;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.briup.smart.env.Configuration;
import com.briup.smart.env.entity.Environment;
import com.briup.smart.env.server.DBStoreImpl;
import com.briup.smart.env.support.ConfigurationAware;
import com.briup.smart.env.support.PropertiesAware;
import com.briup.smart.env.util.Log;

import org.hamcrest.core.Is;
import org.omg.CORBA.PRIVATE_MEMBER;

public class ServerImpl implements Server, ConfigurationAware, PropertiesAware {

	private ExecutorService pool = null;
	private ServerSocket server = null;
	private boolean flag = true;

	private Log logger;
	private DBStore db;

	private int port;
	private int shutdownPort;

	@Override
	public void reciver() throws Exception {

		// 线程池对象， 实现多个客户端并发请求
		pool = Executors.newFixedThreadPool(5);

		server = new ServerSocket(port);
		logger.info("接收服务器开启");
		// main线程进入循环状态
		while (flag) {
			// System.out.println("端口号为9999客户端等待连接");
			logger.info("等待客户端连接");
			// 调用accept方法接收客户端连接
			Socket socket = server.accept();
			// System.out.println("端口号为9999客户端连接成功");
			logger.info("连接成功");
			pool.execute(() -> {

				try {
					// 获取输入流从而获取集合对象
					InputStream is = socket.getInputStream();
					ObjectInputStream ois = new ObjectInputStream(is);

					Collection<com.briup.smart.env.entity.Environment> coll = (Collection<com.briup.smart.env.entity.Environment>) ois
							.readObject();
					String name = Thread.currentThread().getName();
					// System.out.println(name + " 接收到数据：" + coll.size());
					logger.info(name + "接收到数据：" + coll.size());
					db.saveDB(coll);

				} catch (Exception e) {

					e.printStackTrace();

				}

			});
		}
	}
	/*
	 * shutdown 与reciver同时调用
	 *  reciver 阻塞无法shutdown 
	 *  shutdown reciver没有创建服务器
	 */

	@Override
	public void shutdown() throws Exception {
		// 新的线程 创建新的server服务器实现停止服务器
		new Thread(() -> {
			// 创建新的server服务器实现停止服务器
			try {
				ServerSocket server = new ServerSocket(shutdownPort);
				// System.out.println("端口号为8888客户端准备连接，开启停止服务器");
				logger.info("开始关闭服务器");
				// 连接成功，表示停止接收服务器
				server.accept();
				// System.out.println("端口号为8888客户端连接成功开始关闭接收服务器");
				logger.info("开始关闭接收服务器");
				if (this.server != null)
					this.server.close();
				if (this.pool != null)
					this.pool.shutdown();
				this.flag = false;
			} catch (IOException e) {

				e.printStackTrace();
			}

		}).start();

	}

	@Override
	public void init(Properties properties) throws Exception {
		this.port = Integer.parseInt(properties.getProperty("server-port"));
		this.shutdownPort = Integer.parseInt(properties.getProperty("shutdown-port"));

	}

	@Override
	public void setConfiguration(Configuration configuration) throws Exception {
		this.db = configuration.getDbStore();
		this.logger = configuration.getLogger();
	}

	/*
	 * public static void main(String[] args) throws Exception {
	 * 
	 * ServerImpl server = new ServerImpl(); // 开启停止服务器，等待停止指令 server.shutdown();
	 * 
	 * // try { // 接收客户端数据 server.reciver();
	 * 
	 * } catch (Exception e) { System.out.println("关闭服务器成功"); } }
	 */

}
