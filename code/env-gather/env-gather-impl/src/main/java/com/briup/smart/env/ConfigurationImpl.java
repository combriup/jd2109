package com.briup.smart.env;

import static org.hamcrest.CoreMatchers.instanceOf;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.dom4j.Document;

import org.dom4j.*;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import com.alibaba.druid.mock.MockParameterMetaData.Parameter;
import com.briup.smart.env.client.Client;
import com.briup.smart.env.client.Gather;
import com.briup.smart.env.server.DBStore;
import com.briup.smart.env.server.Server;
import com.briup.smart.env.support.ConfigurationAware;
import com.briup.smart.env.support.PropertiesAware;
import com.briup.smart.env.util.Backup;
import com.briup.smart.env.util.Log;

public class ConfigurationImpl implements Configuration {
	// 1创建map集合对象保存其他的模块对象  名称-->实现类对象
	private static Map<String, Object> map;
	// 2创建一个集合对象，保存所有模块的配置信息
	private static Properties props;

	private static Configuration config;

	static {
		// 3属性赋值 map
		map = new HashMap<>();
		props = new Properties();
		// 4通过dom4j解析文档获取集合中的元素信息
		parseXml("conf.xml"); // resources目录
		System.out.println(map);
		System.out.println(props);

		// 5将创建的模块对象进行赋值操作
		initModule();

	}

	// 单例模式 私有构造器 只能创建一个对象
	private ConfigurationImpl() {
		// 整个项目只有一个配置模块对象即可
	}

	// 赋值  初始化模块
	private static void initModule() {

		try {
			Collection<Object> modules = map.values();
			for (Object module : modules) {
				if (module instanceof ConfigurationAware) {
					// 1解决模块之间的依赖
					((ConfigurationAware) module).setConfiguration(instance());
				}
				if (module instanceof PropertiesAware) {
					// 2解决模块之间配置信息的问题
					((PropertiesAware) module).init(props);

				}
			}
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	//单例模式  
	public static Configuration instance() {

		if (config == null) {
			config = new ConfigurationImpl();
		}
		return config;
	}
	// 通过dom4j解析文档获取集合中的元素信息
	private static void parseXml(String path) {

		try {
			InputStream in = ConfigurationImpl.class.getClassLoader().getResourceAsStream(path);
			// 1使用dom4J解析；添加依赖
			SAXReader saxReader = new SAXReader();
			Document document = saxReader.read(in);
			// 2获取根节点： <envirment>
			Element root = document.getRootElement();
			// 3根据根节点获取子节点<gather>
			List<Element> elements = root.elements();
			for (Element moudle : elements) {
				String name = moudle.getName(); // 模块名称
				String className = moudle.attributeValue("class"); //模块的class属性
				Object obj = Class.forName(className).newInstance();
				map.put(name, obj);
				// 4子元素 <port>
				Iterator<Element> iterator = moudle.elementIterator();
				while (iterator.hasNext()) {
					// 获取每个模块需要的参数
					Element param = (Element) iterator.next();
					String key = param.getName();
					String value = param.getTextTrim();
					props.setProperty(key, value);

				}

			}

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Override
	public Log getLogger() throws Exception {

		return (Log) map.get("logger");
	}

	@Override
	public Server getServer() throws Exception {

		return (Server) map.get("server");
	}

	@Override
	public Client getClient() throws Exception {

		return (Client) map.get("client");
	}

	@Override
	public DBStore getDbStore() throws Exception {

		return (DBStore) map.get("dbStore");
	}

	@Override
	public Gather getGather() throws Exception {

		return (Gather) map.get("gather");
	}

	@Override
	public Backup getBackup() throws Exception {

		return (Backup) map.get("backup");
	}
	public static void main(String[] args) {
		
		
		
	}

}
/**
 * 实现配置模块，修改其他模块的代码
 * 1实现2个接口
 * 2实现抽象方法
 * 
 */

